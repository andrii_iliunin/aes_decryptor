package com.nyarian.decryptor

import com.nyarian.decryptor.AESConfiguration.EOF
import com.nyarian.decryptor.AESConfiguration.FILE_ENCRYPTION_ALGORITHM
import com.nyarian.decryptor.AESConfiguration.SECRET_KEY_ALGORITHM
import com.nyarian.decryptor.AESConfiguration.KEYGEN_ALGORITHM
import com.nyarian.decryptor.AESConfiguration.KEY_HASH_ITERATIONS
import com.nyarian.decryptor.AESConfiguration.KEY_SIZE_IN_BITS
import com.nyarian.decryptor.AESConfiguration.READ_BUFFER_SIZE
import org.bouncycastle.jce.provider.BouncyCastleProvider
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.security.Security
import javax.crypto.Cipher
import javax.crypto.CipherInputStream
import javax.crypto.SecretKey
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.PBEKeySpec
import javax.crypto.spec.SecretKeySpec

fun main() {
    val backupPath = "/home/nyarian/Downloads/backup_salted_test"
    val password = "coolpassword"

    Security.setProperty("crypto.policy", "unlimited")
    Security.addProvider(BouncyCastleProvider())

    FileInputStream(File(backupPath)).use { readStream ->
        FileOutputStream(File("./decrypted.zip")
            .apply { if (exists()) delete() })
            .use { writeStream ->
                println("password = ${Binary(password.toByteArray()).asString()}")
                val salt: ByteArray = ByteArray(16).apply { readStream.read(this) }
                println("salt = ${Binary(salt).asString()}")
                val iv: ByteArray = ByteArray(16).apply { readStream.read(this) }
                println("iv = ${Binary(iv).asString()}")
                val key = deriveKey(password.toCharArray(), salt)
                println("key = ${Binary(key.encoded).asString()}")
                decipherFile(key, iv, readStream, writeStream)
        }
    }
}

private fun decipherFile(sks: SecretKeySpec, iv: ByteArray, fis: FileInputStream, fos: FileOutputStream) {
    val cipher = Cipher.getInstance(FILE_ENCRYPTION_ALGORITHM)
        .apply { init(Cipher.DECRYPT_MODE, sks, IvParameterSpec(iv)) }
    val input = CipherInputStream(fis, cipher)
    val buffer = ByteArray(READ_BUFFER_SIZE)
    generateSequence { input.read(buffer) }
        .takeWhile { it != EOF }
        .forEach { fos.write(buffer, 0, it) }
    fos.flush()
}

@Suppress("SameParameterValue")
private fun deriveKey(password: CharArray, salt: ByteArray): SecretKeySpec {
    val spec = PBEKeySpec(password, salt, KEY_HASH_ITERATIONS, KEY_SIZE_IN_BITS)
    val secret: SecretKey = SecretKeyFactory.getInstance(KEYGEN_ALGORITHM).generateSecret(spec)
    return SecretKeySpec(secret.encoded, SECRET_KEY_ALGORITHM)
}

private object AESConfiguration {
    internal const val SECRET_KEY_ALGORITHM = "AES"
    internal const val KEYGEN_ALGORITHM = "PBKDF2withHmacSHA1"
    internal const val FILE_ENCRYPTION_ALGORITHM = "AES/CBC/PKCS5Padding"
    internal const val KEY_HASH_ITERATIONS = 10000
    internal const val KEY_SIZE_IN_BITS = 256
    internal const val EOF = -1
    internal const val READ_BUFFER_SIZE = 1024
}
