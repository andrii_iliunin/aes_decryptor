package com.nyarian.decryptor;

public class Binary {

    private final byte[] bytes;

    public Binary(byte[] bytes) {
        this.bytes = bytes;
    }

    public String asString() {
        final StringBuilder sb = new StringBuilder();
        for (byte aByte : bytes) {
            sb
                    .append(String.format("%8s", Integer.toBinaryString(aByte & 0xFF)).replace(' ', '0'))
                    .append(' ');
        }
        return sb.toString();
    }

}
